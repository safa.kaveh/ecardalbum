package com.ecardalbum.ecard.modeldto.dtomodel;

import com.ecardalbum.ecard.modeldto.DtoModel;

public class Login extends DtoModel {
	private static final long serialVersionUID = 1L;
	private AbstractUser user;
	private UsernamePassword usernamePassword;

	public AbstractUser getUser() {
		return user;
	}

	public void setUser(AbstractUser user) {
		this.user = user;
	}

	public UsernamePassword getUsernamePassword() {
		return usernamePassword;
	}

	public void setUsernamePassword(UsernamePassword usernamePassword) {
		this.usernamePassword = usernamePassword;
	}

}
