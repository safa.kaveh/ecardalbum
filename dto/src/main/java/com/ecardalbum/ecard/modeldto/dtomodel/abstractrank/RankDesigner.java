package com.ecardalbum.ecard.modeldto.dtomodel.abstractrank;

import com.ecardalbum.ecard.modeldto.dtomodel.AbstractRank;
import com.ecardalbum.ecard.modeldto.dtomodel.abstractuser.Designer;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("RankDesigner")
public class RankDesigner extends AbstractRank {
	private static final long serialVersionUID = -6765571022871412051L;
	private Designer designer;

	public Designer getDesigner() {
		return designer;
	}

	public void setDesigner(Designer designer) {
		this.designer = designer;
	}

}
