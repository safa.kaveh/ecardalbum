package com.ecardalbum.ecard.business.service.internal.businessmodel.businessmodelservice;

import com.ecardalbum.ecard.business.service.internal.businessmodel.BusinessModelService;

public class RelUserContact extends
		BusinessModelService<com.ecardalbum.ecard.modeldto.dtomodel.RelUserContact, com.ecardalbum.ecard.dao.entitymodel.entitymodel.RelUserContact> {

	public RelUserContact() {
		super(com.ecardalbum.ecard.modeldto.dtomodel.RelUserContact.class,
				com.ecardalbum.ecard.dao.entitymodel.entitymodel.RelUserContact.class);
	}

}
