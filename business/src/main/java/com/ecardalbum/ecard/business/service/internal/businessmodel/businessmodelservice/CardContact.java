package com.ecardalbum.ecard.business.service.internal.businessmodel.businessmodelservice;

import com.ecardalbum.ecard.business.service.internal.businessmodel.BusinessModelService;

public class CardContact extends
		BusinessModelService<com.ecardalbum.ecard.modeldto.dtomodel.CardContact, com.ecardalbum.ecard.dao.entitymodel.entitymodel.CardContact> {

	public CardContact() {
		super(com.ecardalbum.ecard.modeldto.dtomodel.CardContact.class,
				com.ecardalbum.ecard.dao.entitymodel.entitymodel.CardContact.class);

	}

}
