package com.ecardalbum.ecard.business.service.internal.businessmodel.businessmodelservice.abstractrank;

import com.ecardalbum.ecard.business.service.internal.businessmodel.BusinessModelService;

public class RankDesigner extends
		BusinessModelService<com.ecardalbum.ecard.modeldto.dtomodel.abstractrank.RankDesigner, com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractrank.RankDesigner> {

	public RankDesigner() {
		super(com.ecardalbum.ecard.modeldto.dtomodel.abstractrank.RankDesigner.class,
				com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractrank.RankDesigner.class);
	}

}
