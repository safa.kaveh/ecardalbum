package com.ecardalbum.ecard.dao.entitymodel.entitymodel;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.ecardalbum.ecard.dao.entitymodel.EntityModel;

@Entity
@Table(name = "CARD_CONTACTS")
public class CardContact extends EntityModel {
	private static final long serialVersionUID = 1230901975392245952L;
	private String description;
	private String content;
	private String title;
	private AbstractCard card;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public AbstractCard getCard() {
		return card;
	}

	public void setCard(AbstractCard card) {
		this.card = card;
	}

}
