package com.ecardalbum.ecard.modeldto.dtomodel;

import com.ecardalbum.ecard.modeldto.DtoModel;
import com.ecardalbum.ecard.modeldto.dtomodel.abstractuser.Designer;
import com.ecardalbum.ecard.modeldto.dtomodel.abstractuser.Printary;

public class RelPrintaryDesigner extends DtoModel {
	private static final long serialVersionUID = -8693396765160589803L;
	private Printary printary;
	private Designer designer;

	public Printary getPrintary() {
		return printary;
	}

	public void setPrintary(Printary printary) {
		this.printary = printary;
	}

	public Designer getDesigner() {
		return designer;
	}

	public void setDesigner(Designer designer) {
		this.designer = designer;
	}

}
