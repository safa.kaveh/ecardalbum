package com.ecardalbum.ecard.dao.entitymodel.entitymodel;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.ecardalbum.ecard.dao.entitymodel.EntityModel;

@Entity
@Table(name = "CARD_PICTURES")
public class CardPicture extends EntityModel {
	private static final long serialVersionUID = 8144759363688103767L;

	@Column(name = "PICTURE")
	@Lob
	private byte[] picture;

	@Column(name = "DESCRIPTION")
	private String description;

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_CARD")
	private AbstractCard card;

	public byte[] getPicture() {
		return picture;
	}

	public void setPicture(byte[] picture) {
		this.picture = picture;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public AbstractCard getCard() {
		return card;
	}

	public void setCard(AbstractCard card) {
		this.card = card;
	}

}
