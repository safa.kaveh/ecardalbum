package com.ecardalbum.ecard.modeldto.dtomodel.abstractrank;

import com.ecardalbum.ecard.modeldto.dtomodel.AbstractRank;
import com.ecardalbum.ecard.modeldto.dtomodel.abstractcard.FinalCard;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("RankCard")
public class RankCard extends AbstractRank {
	private static final long serialVersionUID = -3314135921689134280L;
	private FinalCard card;

	public FinalCard getCard() {
		return card;
	}

	public void setCard(FinalCard card) {
		this.card = card;
	}

}
