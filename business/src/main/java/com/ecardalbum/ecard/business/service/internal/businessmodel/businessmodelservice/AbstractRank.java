package com.ecardalbum.ecard.business.service.internal.businessmodel.businessmodelservice;

import com.ecardalbum.ecard.business.service.internal.businessmodel.BusinessModelService;

public abstract class AbstractRank extends
		BusinessModelService<com.ecardalbum.ecard.modeldto.dtomodel.AbstractRank, com.ecardalbum.ecard.dao.entitymodel.entitymodel.AbstractRank> {

	public AbstractRank() {
		super(com.ecardalbum.ecard.modeldto.dtomodel.AbstractRank.class,
				com.ecardalbum.ecard.dao.entitymodel.entitymodel.AbstractRank.class);

	}

}
