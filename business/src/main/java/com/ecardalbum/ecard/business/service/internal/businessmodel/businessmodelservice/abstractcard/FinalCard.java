package com.ecardalbum.ecard.business.service.internal.businessmodel.businessmodelservice.abstractcard;

import com.ecardalbum.ecard.business.service.internal.businessmodel.BusinessModelService;

public class FinalCard extends
		BusinessModelService<com.ecardalbum.ecard.modeldto.dtomodel.abstractcard.FinalCard, com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractcard.FinalCard> {

	public FinalCard() {
		super(com.ecardalbum.ecard.modeldto.dtomodel.abstractcard.FinalCard.class,
				com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractcard.FinalCard.class);
	}

}
