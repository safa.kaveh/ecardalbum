package com.ecardalbum.ecard.modeldto.dtomodel;

import com.ecardalbum.ecard.modeldto.DtoModel;
import com.ecardalbum.ecard.modeldto.dtomodel.abstractuser.Designer;
import com.ecardalbum.ecard.modeldto.dtomodel.abstractuser.Printary;
import com.ecardalbum.ecard.modeldto.dtomodel.abstractuser.User;

public class RelCardDesignerPrintaryUser extends DtoModel {
	private static final long serialVersionUID = 8886389707978716255L;
	private Designer designer;
	private Printary printary;
	private User user;
	private AbstractCard card;

	public Designer getDesigner() {
		return designer;
	}

	public void setDesigner(Designer designer) {
		this.designer = designer;
	}

	public Printary getPrintary() {
		return printary;
	}

	public void setPrintary(Printary printary) {
		this.printary = printary;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public AbstractCard getCard() {
		return card;
	}

	public void setCard(AbstractCard card) {
		this.card = card;
	}

}
