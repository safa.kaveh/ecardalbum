package com.ecardalbum.ecard.business.service.internal.businessmodel.businessmodelservice;

import com.ecardalbum.ecard.business.service.internal.businessmodel.BusinessModelService;

public abstract class AbstractUser extends
		BusinessModelService<com.ecardalbum.ecard.modeldto.dtomodel.AbstractUser, com.ecardalbum.ecard.dao.entitymodel.entitymodel.AbstractUser> {

	public AbstractUser() {
		super(com.ecardalbum.ecard.modeldto.dtomodel.AbstractUser.class,
				com.ecardalbum.ecard.dao.entitymodel.entitymodel.AbstractUser.class);

	}

}
