package com.ecardalbum.ecard.dao.entitymodel.entitymodel;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.ecardalbum.ecard.dao.entitymodel.EntityModel;
import com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractcard.FinalCard;
import com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractuser.User;

@Entity
@Table(name = "CLOUDS")
public class Cloud extends EntityModel {
	private static final long serialVersionUID = 3318355753722776845L;
	
	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	@JoinColumn(name="ID_ABSTRACTUSER")
	private FinalCard card;
	
	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	@JoinColumn(name="ID_ABSTRACTUSER")
	private User user;
	
	@Column(name = "CLOUD_CURRENT_SIZE")
	private long cloudCurrentSize;

	@Column(name = "CLOUD_LIMIT_SIZE")
	private long cloudLimitSize;

	public FinalCard getCard() {
		return card;
	}

	public void setCard(FinalCard card) {
		this.card = card;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public long getCloudCurrentSize() {
		return cloudCurrentSize;
	}

	public void setCloudCurrentSize(long cloudCurrentSize) {
		this.cloudCurrentSize = cloudCurrentSize;
	}

	public long getCloudLimitSize() {
		return cloudLimitSize;
	}

	public void setCloudLimitSize(long cloudLimitSize) {
		this.cloudLimitSize = cloudLimitSize;
	}

}
