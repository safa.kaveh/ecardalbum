package com.ecardalbum.ecard.dao.entitymodel.entitymodel;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.ecardalbum.ecard.dao.entitymodel.EntityModel;

@Entity
@Table(name = "REL_USER_CONTACTS")
public class RelUserContact extends EntityModel {
	private static final long serialVersionUID = 1L;

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	@JoinColumn(name="ID_ABSTRACTUSER")
	private AbstractUser abstractUser;

	@OneToOne(cascade = CascadeType.PERSIST, fetch = FetchType.EAGER)
	@JoinColumn(name="ID_CONTACT")
	private Contact contact;

	public AbstractUser getAbstractUser() {
		return abstractUser;
	}

	public void setAbstractUser(AbstractUser abstractUser) {
		this.abstractUser = abstractUser;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

}
