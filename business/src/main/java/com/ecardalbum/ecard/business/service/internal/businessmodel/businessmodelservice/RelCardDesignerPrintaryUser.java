package com.ecardalbum.ecard.business.service.internal.businessmodel.businessmodelservice;

import com.ecardalbum.ecard.business.service.internal.businessmodel.BusinessModelService;

public class RelCardDesignerPrintaryUser extends
		BusinessModelService<com.ecardalbum.ecard.modeldto.dtomodel.RelCardDesignerPrintaryUser, com.ecardalbum.ecard.dao.entitymodel.entitymodel.RelCardDesignerPrintaryUser> {

	public RelCardDesignerPrintaryUser() {
		super(com.ecardalbum.ecard.modeldto.dtomodel.RelCardDesignerPrintaryUser.class,
				com.ecardalbum.ecard.dao.entitymodel.entitymodel.RelCardDesignerPrintaryUser.class);

	}

}
