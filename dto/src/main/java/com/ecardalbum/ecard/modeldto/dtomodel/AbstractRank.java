package com.ecardalbum.ecard.modeldto.dtomodel;

import com.ecardalbum.ecard.modeldto.DtoModel;
import com.ecardalbum.ecard.modeldto.dtomodel.abstractrank.RankCard;
import com.ecardalbum.ecard.modeldto.dtomodel.abstractrank.RankDesigner;
import com.ecardalbum.ecard.modeldto.dtomodel.abstractrank.RankPrintary;
import com.ecardalbum.ecard.modeldto.dtomodel.abstractuser.User;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

@JsonTypeInfo(use = Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @JsonSubTypes.Type(value = RankCard.class, name = "RankCard"),
		@JsonSubTypes.Type(value = RankDesigner.class, name = "RankDesigner"),
		@JsonSubTypes.Type(value = RankPrintary.class, name = "RankPrintary") })
public abstract class AbstractRank extends DtoModel {
	private static final long serialVersionUID = 1L;
	private byte rank;
	private String comment;
	private User user;

	public byte getRank() {
		return rank;
	}

	public void setRank(byte rank) {
		this.rank = rank;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
