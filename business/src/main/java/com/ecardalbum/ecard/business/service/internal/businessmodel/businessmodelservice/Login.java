package com.ecardalbum.ecard.business.service.internal.businessmodel.businessmodelservice;

import com.ecardalbum.ecard.business.service.internal.businessmodel.BusinessModelService;

public class Login extends
		BusinessModelService<com.ecardalbum.ecard.modeldto.dtomodel.Login, com.ecardalbum.ecard.dao.entitymodel.entitymodel.Login> {

	public Login() {
		super(com.ecardalbum.ecard.modeldto.dtomodel.Login.class,
				com.ecardalbum.ecard.dao.entitymodel.entitymodel.Login.class);

	}
}
