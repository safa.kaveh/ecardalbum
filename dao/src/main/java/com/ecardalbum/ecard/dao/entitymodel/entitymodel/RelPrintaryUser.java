package com.ecardalbum.ecard.dao.entitymodel.entitymodel;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.ecardalbum.ecard.dao.entitymodel.EntityModel;
import com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractuser.Printary;
import com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractuser.User;

@Entity
@Table(name = "REL_PRINTARY_USERS")
public class RelPrintaryUser extends EntityModel {
	private static final long serialVersionUID = -8847751248282366384L;
	@OneToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_PRINTARY")
	private Printary printary;

	@OneToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_USER")
	private User user;

	public Printary getPrintary() {
		return printary;
	}

	public void setPrintary(Printary printary) {
		this.printary = printary;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
