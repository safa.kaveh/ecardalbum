package com.ecardalbum.ecard.business.service.internal.businessmodel.businessmodelservice;

import com.ecardalbum.ecard.business.service.internal.businessmodel.BusinessModelService;

public class UsernamePassword extends
		BusinessModelService<com.ecardalbum.ecard.modeldto.dtomodel.UsernamePassword, com.ecardalbum.ecard.dao.entitymodel.entitymodel.UsernamePassword> {

	public UsernamePassword() {
		super(com.ecardalbum.ecard.modeldto.dtomodel.UsernamePassword.class,
				com.ecardalbum.ecard.dao.entitymodel.entitymodel.UsernamePassword.class);
	}

}
