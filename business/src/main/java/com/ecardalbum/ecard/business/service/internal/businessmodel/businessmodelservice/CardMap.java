package com.ecardalbum.ecard.business.service.internal.businessmodel.businessmodelservice;

import com.ecardalbum.ecard.business.service.internal.businessmodel.BusinessModelService;

public class CardMap extends
		BusinessModelService<com.ecardalbum.ecard.modeldto.dtomodel.CardMap, com.ecardalbum.ecard.dao.entitymodel.entitymodel.CardMap> {

	public CardMap() {
		super(com.ecardalbum.ecard.modeldto.dtomodel.CardMap.class,
				com.ecardalbum.ecard.dao.entitymodel.entitymodel.CardMap.class);

	}

}
