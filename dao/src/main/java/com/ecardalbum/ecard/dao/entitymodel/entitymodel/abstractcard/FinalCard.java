package com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractcard;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.ecardalbum.ecard.dao.entitymodel.entitymodel.AbstractCard;

@Entity
@Table(name = "FINAL_CARDS")
public class FinalCard extends AbstractCard {
	private static final long serialVersionUID = 7421756193164859630L;

}
