package com.ecardalbum.ecard.modeldto.dtomodel.abstractcard;

import com.ecardalbum.ecard.modeldto.dtomodel.AbstractCard;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("FinalCard")
public class FinalCard extends AbstractCard {
	private static final long serialVersionUID = 7421756193164859630L;

}
