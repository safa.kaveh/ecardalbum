package com.ecardalbum.ecard.business.service.internal.businessmodel.businessmodelservice.abstractuser;

import com.ecardalbum.ecard.business.service.internal.businessmodel.BusinessModelService;

public class Printary extends
		BusinessModelService<com.ecardalbum.ecard.modeldto.dtomodel.abstractuser.Printary, com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractuser.Printary> {

	public Printary() {
		super(com.ecardalbum.ecard.modeldto.dtomodel.abstractuser.Printary.class,
				com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractuser.Printary.class);
	}

}
