package com.ecardalbum.ecard.business.service.internal.businessmodel.businessmodelservice;

import com.ecardalbum.ecard.business.service.internal.businessmodel.BusinessModelService;

public class RelPrintaryUser extends
		BusinessModelService<com.ecardalbum.ecard.modeldto.dtomodel.RelPrintaryUser, com.ecardalbum.ecard.dao.entitymodel.entitymodel.RelPrintaryUser> {

	public RelPrintaryUser() {
		super(com.ecardalbum.ecard.modeldto.dtomodel.RelPrintaryUser.class,
				com.ecardalbum.ecard.dao.entitymodel.entitymodel.RelPrintaryUser.class);

	}

}
