package com.ecardalbum.ecard.dao.entitymodel;

import java.io.IOException;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Version;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@MappedSuperclass
public abstract class EntityModel implements Serializable {
	private static final long serialVersionUID = 5920491796222453331L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	protected long id;

	@Version
	@Column(name = "VERSION")
	protected long version;

	@Column(name = "CREATE_ON")
	protected long createOn;

	@Column(name = "UPDATE_ON")
	protected long updateOn;

	@Column(name = "CREATE_USER_ID")
	protected long createUserId;

	@Column(name = "UPDATE_USER_ID")
	protected long updateUserId;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getVersion() {
		return version;
	}

	public void setVersion(long version) {
		this.version = version;
	}

	public long getCreateOn() {
		return createOn;
	}

	public void setCreateOn(long createOn) {
		this.createOn = createOn;
	}

	public long getUpdateOn() {
		return updateOn;
	}

	public void setUpdateOn(long updateOn) {
		this.updateOn = updateOn;
	}

	public long getCreateUserId() {
		return createUserId;
	}

	public void setCreateUserId(long createUserId) {
		this.createUserId = createUserId;
	}

	public long getUpdateUserId() {
		return updateUserId;
	}

	public void setUpdateUserId(long updateUserId) {
		this.updateUserId = updateUserId;
	}

	public static <M extends EntityModel> M getFromJsonString(String json, Class<M> clazz) {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.readValue(json, clazz);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	@Override
	public String toString() {
		ObjectMapper mapper = new ObjectMapper();
		try {
			return mapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return "";
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		return super.clone();
	}

}
