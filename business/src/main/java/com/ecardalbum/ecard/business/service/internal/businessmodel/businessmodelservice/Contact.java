package com.ecardalbum.ecard.business.service.internal.businessmodel.businessmodelservice;

import com.ecardalbum.ecard.business.service.internal.businessmodel.BusinessModelService;

public class Contact extends
		BusinessModelService<com.ecardalbum.ecard.modeldto.dtomodel.Contact, com.ecardalbum.ecard.dao.entitymodel.entitymodel.Contact> {

	public Contact() {
		super(com.ecardalbum.ecard.modeldto.dtomodel.Contact.class,
				com.ecardalbum.ecard.dao.entitymodel.entitymodel.Contact.class);

	}

}
