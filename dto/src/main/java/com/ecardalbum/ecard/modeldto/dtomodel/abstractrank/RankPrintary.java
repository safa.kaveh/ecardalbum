package com.ecardalbum.ecard.modeldto.dtomodel.abstractrank;

import com.ecardalbum.ecard.modeldto.dtomodel.AbstractRank;
import com.ecardalbum.ecard.modeldto.dtomodel.abstractuser.Printary;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("RankPrintary")
public class RankPrintary extends AbstractRank {
	private static final long serialVersionUID = -2944189139857476302L;
	private Printary printary;

	public Printary getPrintary() {
		return printary;
	}

	public void setPrintary(Printary printary) {
		this.printary = printary;
	}

}
