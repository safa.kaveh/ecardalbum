package com.ecardalbum.ecard.modeldto.dtomodel.abstractcard;

import com.ecardalbum.ecard.modeldto.dtomodel.AbstractCard;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("TempCard")
public class TempCard extends AbstractCard {
	private static final long serialVersionUID = 2289291015570445534L;
	private String rejectDescription;
	private boolean accept;
	private long createTime;
	private long rejectOrAcceptTime;

	public String getRejectDescription() {
		return rejectDescription;
	}

	public void setRejectDescription(String rejectDescription) {
		this.rejectDescription = rejectDescription;
	}

	public boolean isAccept() {
		return accept;
	}

	public void setAccept(boolean accept) {
		this.accept = accept;
	}

	public long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(long createTime) {
		this.createTime = createTime;
	}

	public long getRejectOrAcceptTime() {
		return rejectOrAcceptTime;
	}

	public void setRejectOrAcceptTime(long rejectOrAcceptTime) {
		this.rejectOrAcceptTime = rejectOrAcceptTime;
	}

}
