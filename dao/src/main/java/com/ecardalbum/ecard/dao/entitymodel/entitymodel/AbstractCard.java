package com.ecardalbum.ecard.dao.entitymodel.entitymodel;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.ecardalbum.ecard.dao.entitymodel.EntityModel;
import com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractcard.FinalCard;
import com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractcard.TempCard;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

@Entity
@Table(name = "ABSTRACT_CARDS")
@JsonTypeInfo(use = Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @JsonSubTypes.Type(value = FinalCard.class, name = "FinalCard"),
		@JsonSubTypes.Type(value = TempCard.class, name = "TempCard") })
public abstract class AbstractCard extends EntityModel {
	private static final long serialVersionUID = 1550041286692605218L;
	private String qrcode;
	private byte[] cardPicture;
	private byte[] video;
	private byte[] audio;
	private String text;

	public String getQrcode() {
		return qrcode;
	}

	public void setQrcode(String qrcode) {
		this.qrcode = qrcode;
	}

	public byte[] getCardPicture() {
		return cardPicture;
	}

	public void setCardPicture(byte[] cardPicture) {
		this.cardPicture = cardPicture;
	}

	public byte[] getVideo() {
		return video;
	}

	public void setVideo(byte[] video) {
		this.video = video;
	}

	public byte[] getAudio() {
		return audio;
	}

	public void setAudio(byte[] audio) {
		this.audio = audio;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
