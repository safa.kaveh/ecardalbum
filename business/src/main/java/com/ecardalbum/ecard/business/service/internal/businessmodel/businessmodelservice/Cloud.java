package com.ecardalbum.ecard.business.service.internal.businessmodel.businessmodelservice;

import com.ecardalbum.ecard.business.service.internal.businessmodel.BusinessModelService;

public class Cloud extends
		BusinessModelService<com.ecardalbum.ecard.modeldto.dtomodel.Cloud, com.ecardalbum.ecard.dao.entitymodel.entitymodel.Cloud> {

	public Cloud() {
		super(com.ecardalbum.ecard.modeldto.dtomodel.Cloud.class,
				com.ecardalbum.ecard.dao.entitymodel.entitymodel.Cloud.class);

	}

}
