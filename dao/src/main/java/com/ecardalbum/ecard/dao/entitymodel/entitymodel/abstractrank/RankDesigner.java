package com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractrank;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.ecardalbum.ecard.dao.entitymodel.entitymodel.AbstractRank;
import com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractuser.Designer;

@Entity
@Table(name = "RANK_DESIGNERS")
public class RankDesigner extends AbstractRank {
	private static final long serialVersionUID = -6765571022871412051L;
	private Designer designer;

	public Designer getDesigner() {
		return designer;
	}

	public void setDesigner(Designer designer) {
		this.designer = designer;
	}

}
