package com.ecardalbum.ecard.modeldto.dtomodel.abstractuser;

import com.ecardalbum.ecard.modeldto.dtomodel.AbstractUser;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("User")
public class User extends AbstractUser {
	private static final long serialVersionUID = 1L;

}
