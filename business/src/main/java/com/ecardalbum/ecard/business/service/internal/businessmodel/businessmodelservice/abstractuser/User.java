package com.ecardalbum.ecard.business.service.internal.businessmodel.businessmodelservice.abstractuser;

import com.ecardalbum.ecard.business.service.internal.businessmodel.BusinessModelService;

public class User extends
		BusinessModelService<com.ecardalbum.ecard.modeldto.dtomodel.abstractuser.User, com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractuser.User> {

	public User() {
		super(com.ecardalbum.ecard.modeldto.dtomodel.abstractuser.User.class,
				com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractuser.User.class);
	}

}
