package com.ecardalbum.ecard.modeldto.dtomodel.abstractuser;

import com.ecardalbum.ecard.modeldto.dtomodel.AbstractUser;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("Printary")
public class Printary extends AbstractUser {
	private static final long serialVersionUID = 1L;

}
