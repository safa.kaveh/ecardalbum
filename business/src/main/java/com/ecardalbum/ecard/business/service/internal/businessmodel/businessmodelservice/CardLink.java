package com.ecardalbum.ecard.business.service.internal.businessmodel.businessmodelservice;

import com.ecardalbum.ecard.business.service.internal.businessmodel.BusinessModelService;

public class CardLink extends
		BusinessModelService<com.ecardalbum.ecard.modeldto.dtomodel.CardLink, com.ecardalbum.ecard.dao.entitymodel.entitymodel.CardLink> {

	public CardLink() {
		super(com.ecardalbum.ecard.modeldto.dtomodel.CardLink.class,
				com.ecardalbum.ecard.dao.entitymodel.entitymodel.CardLink.class);

	}

}
