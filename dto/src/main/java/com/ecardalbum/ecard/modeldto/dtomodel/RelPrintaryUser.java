package com.ecardalbum.ecard.modeldto.dtomodel;

import com.ecardalbum.ecard.modeldto.DtoModel;
import com.ecardalbum.ecard.modeldto.dtomodel.abstractuser.Printary;
import com.ecardalbum.ecard.modeldto.dtomodel.abstractuser.User;

public class RelPrintaryUser extends DtoModel {
	private static final long serialVersionUID = -8847751248282366384L;
	private Printary printary;
	private User user;

	public Printary getPrintary() {
		return printary;
	}

	public void setPrintary(Printary printary) {
		this.printary = printary;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
