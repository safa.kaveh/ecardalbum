package com.ecardalbum.ecard.dao.entitymodel.entitymodel;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.ecardalbum.ecard.dao.entitymodel.EntityModel;
import com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractrank.RankCard;
import com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractrank.RankDesigner;
import com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractrank.RankPrintary;
import com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractuser.User;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

@Entity
@Table(name = "ABSTRACT_RANKS")
@JsonTypeInfo(use = Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @JsonSubTypes.Type(value = RankCard.class, name = "RankCard"),
		@JsonSubTypes.Type(value = RankDesigner.class, name = "RankDesigner"),
		@JsonSubTypes.Type(value = RankPrintary.class, name = "RankPrintary") })
public abstract class AbstractRank extends EntityModel {
	private static final long serialVersionUID = 1L;
	private byte rank;
	private String comment;
	private User user;

	public byte getRank() {
		return rank;
	}

	public void setRank(byte rank) {
		this.rank = rank;
	}

	public String getComment() {
		return comment;
	}

	public void setComment(String comment) {
		this.comment = comment;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
