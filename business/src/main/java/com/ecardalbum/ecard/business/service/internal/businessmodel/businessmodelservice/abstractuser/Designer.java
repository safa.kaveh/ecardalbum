package com.ecardalbum.ecard.business.service.internal.businessmodel.businessmodelservice.abstractuser;

import com.ecardalbum.ecard.business.service.internal.businessmodel.BusinessModelService;

public class Designer extends
		BusinessModelService<com.ecardalbum.ecard.modeldto.dtomodel.abstractuser.Designer, com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractuser.Designer> {

	public Designer() {
		super(com.ecardalbum.ecard.modeldto.dtomodel.abstractuser.Designer.class,
				com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractuser.Designer.class);
	}

}
