package com.ecardalbum.ecard.modeldto.dtomodel.abstractuser;

import com.ecardalbum.ecard.modeldto.dtomodel.AbstractUser;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("Admin")
public class Admin extends AbstractUser {
	private static final long serialVersionUID = 2288893838940537642L;

}
