package com.ecardalbum.ecard.business.service.internal.businessmodel.businessmodelservice.abstractcard;

import com.ecardalbum.ecard.business.service.internal.businessmodel.BusinessModelService;

public class TempCard extends
		BusinessModelService<com.ecardalbum.ecard.modeldto.dtomodel.abstractcard.TempCard, com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractcard.TempCard> {

	public TempCard() {
		super(com.ecardalbum.ecard.modeldto.dtomodel.abstractcard.TempCard.class,
				com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractcard.TempCard.class);
	}

}
