package com.ecardalbum.ecard.dao.entitymodel.entitymodel;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.ecardalbum.ecard.dao.entitymodel.EntityModel;

@Entity
@Table(name = "USERNAME_PASSWORDS")
public class UsernamePassword extends EntityModel {
	private static final long serialVersionUID = 7962761131588144083L;

	@Column(name = "USERNAME")
	private String username;

	@Column(name = "PASWORD")
	private String pasword;

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPasword() {
		return pasword;
	}

	public void setPasword(String pasword) {
		this.pasword = pasword;
	}

}
