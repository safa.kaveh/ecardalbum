package com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractrank;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.ecardalbum.ecard.dao.entitymodel.entitymodel.AbstractRank;
import com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractuser.Printary;

@Entity
@Table(name = "RANK_PRINTARYS")
public class RankPrintary extends AbstractRank {
	private static final long serialVersionUID = -2944189139857476302L;
	private Printary printary;

	public Printary getPrintary() {
		return printary;
	}

	public void setPrintary(Printary printary) {
		this.printary = printary;
	}

}
