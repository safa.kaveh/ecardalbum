package com.ecardalbum.ecard.modeldto.dtomodel;

import com.ecardalbum.ecard.modeldto.DtoModel;

public class RelUserContact extends DtoModel {
	private static final long serialVersionUID = 1L;
	private AbstractUser abstractUser;
	private Contact contact;

	public AbstractUser getAbstractUser() {
		return abstractUser;
	}

	public void setAbstractUser(AbstractUser abstractUser) {
		this.abstractUser = abstractUser;
	}

	public Contact getContact() {
		return contact;
	}

	public void setContact(Contact contact) {
		this.contact = contact;
	}

}
