package com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractuser;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.ecardalbum.ecard.dao.entitymodel.entitymodel.AbstractUser;

@Entity
@Table(name = "REL_USER_CONTACTS")
public class User extends AbstractUser {
	private static final long serialVersionUID = 1L;

}
