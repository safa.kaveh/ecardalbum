package com.ecardalbum.ecard.dao.entitymodel.entitymodel;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.ecardalbum.ecard.dao.entitymodel.EntityModel;
import com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractuser.Designer;
import com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractuser.Printary;
import com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractuser.User;

@Entity
@Table(name = "REL_CARDDESIGNER_PRINTARY_USERS")
public class RelCardDesignerPrintaryUser extends EntityModel {
	private static final long serialVersionUID = 8886389707978716255L;

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_DESIGNER")
	private Designer designer;

	@OneToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_PRINTARY")
	private Printary printary;

	@OneToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_USER")
	private User user;

	@OneToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_CARD")
	private AbstractCard card;

	public Designer getDesigner() {
		return designer;
	}

	public void setDesigner(Designer designer) {
		this.designer = designer;
	}

	public Printary getPrintary() {
		return printary;
	}

	public void setPrintary(Printary printary) {
		this.printary = printary;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public AbstractCard getCard() {
		return card;
	}

	public void setCard(AbstractCard card) {
		this.card = card;
	}

}
