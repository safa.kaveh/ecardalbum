package com.ecardalbum.ecard.business.service.internal.businessmodel.businessmodelservice.abstractrank;

import com.ecardalbum.ecard.business.service.internal.businessmodel.BusinessModelService;

public class RankPrintary extends
		BusinessModelService<com.ecardalbum.ecard.modeldto.dtomodel.abstractrank.RankPrintary, com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractrank.RankPrintary> {

	public RankPrintary() {
		super(com.ecardalbum.ecard.modeldto.dtomodel.abstractrank.RankPrintary.class,
				com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractrank.RankPrintary.class);
	}

}
