package com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractuser;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.ecardalbum.ecard.dao.entitymodel.entitymodel.AbstractUser;

@Entity
@Table(name = "ADMINS")
public class Admin extends AbstractUser {
	private static final long serialVersionUID = 2288893838940537642L;

}
