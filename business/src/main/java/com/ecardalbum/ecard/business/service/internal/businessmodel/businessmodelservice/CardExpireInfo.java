package com.ecardalbum.ecard.business.service.internal.businessmodel.businessmodelservice;

import com.ecardalbum.ecard.business.service.internal.businessmodel.BusinessModelService;

public class CardExpireInfo extends
		BusinessModelService<com.ecardalbum.ecard.modeldto.dtomodel.CardExpireInfo, com.ecardalbum.ecard.dao.entitymodel.entitymodel.CardExpireInfo> {

	public CardExpireInfo() {
		super(com.ecardalbum.ecard.modeldto.dtomodel.CardExpireInfo.class,
				com.ecardalbum.ecard.dao.entitymodel.entitymodel.CardExpireInfo.class);

	}

}
