package com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractuser;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.ecardalbum.ecard.dao.entitymodel.entitymodel.AbstractUser;

@Entity
@Table(name = "DESIGNERS")
public class Designer extends AbstractUser {
	private static final long serialVersionUID = -2378135597843653577L;

}
