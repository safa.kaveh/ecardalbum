package com.ecardalbum.ecard.modeldto.dtomodel;

import com.ecardalbum.ecard.modeldto.DtoModel;
import com.ecardalbum.ecard.modeldto.dtomodel.abstractcard.FinalCard;
import com.ecardalbum.ecard.modeldto.dtomodel.abstractuser.User;

public class Cloud extends DtoModel {
	private static final long serialVersionUID = 3318355753722776845L;
	private FinalCard card;
	private User user;
	private long cloudCurrentSize;
	private long cloudLimitSize;

	public FinalCard getCard() {
		return card;
	}

	public void setCard(FinalCard card) {
		this.card = card;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public long getCloudCurrentSize() {
		return cloudCurrentSize;
	}

	public void setCloudCurrentSize(long cloudCurrentSize) {
		this.cloudCurrentSize = cloudCurrentSize;
	}

	public long getCloudLimitSize() {
		return cloudLimitSize;
	}

	public void setCloudLimitSize(long cloudLimitSize) {
		this.cloudLimitSize = cloudLimitSize;
	}

}
