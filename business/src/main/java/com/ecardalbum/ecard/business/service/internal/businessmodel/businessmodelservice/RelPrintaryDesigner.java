package com.ecardalbum.ecard.business.service.internal.businessmodel.businessmodelservice;

import com.ecardalbum.ecard.business.service.internal.businessmodel.BusinessModelService;

public class RelPrintaryDesigner extends
		BusinessModelService<com.ecardalbum.ecard.modeldto.dtomodel.RelPrintaryDesigner, com.ecardalbum.ecard.dao.entitymodel.entitymodel.RelPrintaryDesigner> {

	public RelPrintaryDesigner() {
		super(com.ecardalbum.ecard.modeldto.dtomodel.RelPrintaryDesigner.class,
				com.ecardalbum.ecard.dao.entitymodel.entitymodel.RelPrintaryDesigner.class);

	}

}
