package com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractrank;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.ecardalbum.ecard.dao.entitymodel.entitymodel.AbstractRank;
import com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractcard.FinalCard;

@Entity
@Table(name = "RANK_CARDS")
public class RankCard extends AbstractRank {
	private static final long serialVersionUID = -3314135921689134280L;
	private FinalCard card;

	public FinalCard getCard() {
		return card;
	}

	public void setCard(FinalCard card) {
		this.card = card;
	}

}
