package com.ecardalbum.ecard.business.service.internal.businessmodel.businessmodelservice.abstractrank;

import com.ecardalbum.ecard.business.service.internal.businessmodel.BusinessModelService;

public class RankCard extends
		BusinessModelService<com.ecardalbum.ecard.modeldto.dtomodel.abstractrank.RankCard, com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractrank.RankCard> {

	public RankCard() {
		super(com.ecardalbum.ecard.modeldto.dtomodel.abstractrank.RankCard.class,
				com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractrank.RankCard.class);
	}

}
