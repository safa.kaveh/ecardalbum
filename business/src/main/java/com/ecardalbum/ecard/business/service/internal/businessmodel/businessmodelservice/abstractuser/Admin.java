package com.ecardalbum.ecard.business.service.internal.businessmodel.businessmodelservice.abstractuser;

import com.ecardalbum.ecard.business.service.internal.businessmodel.BusinessModelService;

public class Admin extends
		BusinessModelService<com.ecardalbum.ecard.modeldto.dtomodel.abstractuser.Admin, com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractuser.Admin> {

	public Admin() {
		super(com.ecardalbum.ecard.modeldto.dtomodel.abstractuser.Admin.class,
				com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractuser.Admin.class);
	}

}
