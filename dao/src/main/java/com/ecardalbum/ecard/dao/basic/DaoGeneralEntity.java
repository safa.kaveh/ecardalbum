package com.ecardalbum.ecard.dao.basic;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;
import javax.persistence.Query;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.ecardalbum.ecard.dao.entitymodel.EntityModel;

public class DaoGeneralEntity<E extends EntityModel> implements Serializable {
	private static final Logger LOGGER = LogManager.getLogger();
	private static final long serialVersionUID = -6203119142016621971L;

	private EntityManager entityManager;

	public DaoGeneralEntity() {
		EntityManagerFactory emf = Persistence.createEntityManagerFactory("dao");
		this.entityManager = emf.createEntityManager();
		LOGGER.info("EntityManager Create Successfully.");
	}

	public E save(E e) throws Exception {
		if (e == null) {
			LOGGER.info("Your object is null in saved method.");
			return null;
		}
		try {
			entityManager.getTransaction().begin();
			entityManager.persist(e);
			entityManager.flush();
			entityManager.getTransaction().commit();
			LOGGER.info(e.getId() + "Saved Successfully.");
		} catch (Exception ex) {
			LOGGER.error("Can not saved successfully: " + ex.getMessage());
			entityManager.getTransaction().rollback();
			throw ex;
		} finally {
			entityManager.clear();
		}
		return e;
	}

	public List<E> save(List<E> es) throws Exception {
		if (es == null) {
			LOGGER.info("Your list is null in saved method.");
			return null;
		}
		List<E> list = new ArrayList<>();
		for (E e : es) {
			e = save(e);
			if (e.getId() != 0) {
				list.add(e);
			} else {
				LOGGER.info(e.getClass().getSimpleName() + " reject from save list.");
			}
		}
		LOGGER.info("Your list saved.");
		return list;
	}

	public List<E> saveInOneTransaction(List<E> es) throws Exception {
		if (es == null) {
			LOGGER.info("Your list is null in saved method.");
			return null;
		}
		List<E> list = new ArrayList<>();

		try {
			entityManager.getTransaction().begin();
			for (E e : es) {
				entityManager.persist(e);
				list.add(e);
			}
			entityManager.flush();
			entityManager.getTransaction().commit();
			LOGGER.info("All list saved in one transaction successfully.");

		} catch (Exception ex) {
			entityManager.getTransaction().rollback();
			LOGGER.info("can't save transaction rollbak :" + ex.getMessage());
			throw ex;
		} finally {
			entityManager.clear();
		}
		return list;
	}

	public E update(E e) throws Exception {
		if (e == null) {
			LOGGER.info("Your object is null in update method.");
			return null;
		}

		try {
			entityManager.getTransaction().begin();
			entityManager.merge(entityManager.getReference(e.getClass(), ((EntityModel) e).getId()));
			entityManager.merge(e);
			entityManager.flush();
			entityManager.getTransaction().commit();
			LOGGER.info(e.getId() + "Update Successfully.");
		} catch (Exception ex) {
			LOGGER.error("Can not update successfully: " + ex.getMessage());
			entityManager.getTransaction().rollback();
			throw ex;
		} finally {
			entityManager.detach(e);
			entityManager.clear();
		}
		return e;
	}

	public List<E> update(List<E> es) throws Exception {
		if (es == null) {
			LOGGER.info("Your list is null in update method.");
			return null;
		}

		List<E> list = new ArrayList<>();
		for (E e : es) {
			e = update(e);
			if (e.getId() != 0) {
				list.add(e);
			} else {
				LOGGER.info(e.getClass().getSimpleName() + " reject from update list.");
			}
		}
		LOGGER.info("Your list uptodated.");
		return list;
	}

	public List<E> updateInOneTransaction(List<E> ts) throws Exception {
		if (ts == null) {
			LOGGER.info("Your list is null in update method.");
			return null;
		}
		List<E> list = new ArrayList<>();

		try {
			entityManager.getTransaction().begin();
			for (E e : ts) {
				entityManager.merge(entityManager.getReference(e.getClass(), ((EntityModel) e).getId()));
				entityManager.merge(e);
				list.add(e);
			}
			entityManager.flush();
			entityManager.getTransaction().commit();
			LOGGER.info("All list update in one transaction successfully.");

		} catch (Exception ex) {
			entityManager.getTransaction().rollback();
			LOGGER.info("can't update transaction rollbak :" + ex.getMessage());
			throw ex;
		} finally {
			entityManager.clear();
		}
		return list;
	}

	public E delete(E e) throws Exception {
		if (e == null) {
			LOGGER.info("Your object is null in delete method.");
			return null;
		}
		try {
			entityManager.getTransaction().begin();
			entityManager.remove(entityManager.getReference(e.getClass(), ((EntityModel) e).getId()));
			entityManager.flush();
			entityManager.getTransaction().commit();
			LOGGER.info(e.getId() + "Delete Successfully.");
		} catch (Exception ex) {
			LOGGER.error("Can not delete successfully: " + ex.getMessage());
			entityManager.getTransaction().rollback();
			throw ex;
		} finally {
			entityManager.clear();
		}
		return e;
	}

	public List<E> delete(List<E> ts) throws Exception {
		if (ts == null) {
			LOGGER.info("Your list is null in delete method.");
			return null;
		}
		List<E> list = new ArrayList<>();
		for (E e : ts) {
			e = delete(e);
			if (e.getId() != 0) {
				list.add(e);
			} else {
				LOGGER.info(e.getClass().getSimpleName() + " reject from delete list.");
			}
		}
		LOGGER.info("Your list deleted.");
		return list;
	}

	public List<E> deleteInOneTransaction(List<E> ts) throws Exception {
		if (ts == null) {
			LOGGER.info("Your list is null in delete method.");
			return null;
		}
		List<E> list = new ArrayList<>();

		try {
			entityManager.getTransaction().begin();
			for (E e : ts) {
				entityManager.remove(entityManager.getReference(e.getClass(), ((EntityModel) e).getId()));
				list.add(e);
			}
			entityManager.flush();
			entityManager.getTransaction().commit();
			LOGGER.info("All list deleted in one transaction successfully.");

		} catch (Exception ex) {
			entityManager.getTransaction().rollback();
			LOGGER.info("can't delete transaction rollbak :" + ex.getMessage());
			throw ex;
		} finally {
			entityManager.clear();
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	public List<E> select(String queryName, Map<String, Object> params) throws Exception {
		List<E> result = new ArrayList<>();
		Query query = entityManager.createNamedQuery(queryName);
		if (params != null) {
			params.entrySet().forEach(p -> query.setParameter(p.getKey(), p.getValue()));
		}
		entityManager.getTransaction().begin();
		query.setHint("javax.persistence.cache.storeMode", "REFRESH");
		result = query.getResultList();
		entityManager.flush();
		entityManager.clear();
		entityManager.getTransaction().commit();
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<Object> selectMultiType(String queryName, Map<String, Object> params) throws Exception {
		List<Object> result = new ArrayList<>();
		Query query = entityManager.createNamedQuery(queryName);
		if (params != null) {
			params.entrySet().forEach(p -> query.setParameter(p.getKey(), p.getValue()));
		}
		entityManager.getTransaction().begin();
		query.setHint("javax.persistence.cache.storeMode", "REFRESH");
		result = query.getResultList();
		entityManager.flush();
		entityManager.clear();
		entityManager.getTransaction().commit();
		return result;
	}

	public Object selectOneObject(String queryName, Map<String, Object> params) throws Exception {
		Object result = new ArrayList<>();
		Query query = entityManager.createNamedQuery(queryName);
		if (params != null) {
			params.entrySet().forEach(p -> query.setParameter(p.getKey(), p.getValue()));
		}
		entityManager.getTransaction().begin();
		query.setHint("javax.persistence.cache.storeMode", "REFRESH");
		result = query.getSingleResult();
		entityManager.flush();
		entityManager.clear();
		entityManager.getTransaction().commit();
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<E> select(String queryName, Map<String, Object> params, int first, int max) throws Exception {
		List<E> result = new ArrayList<>();
		Query query = entityManager.createNamedQuery(queryName);
		if (params != null) {
			params.entrySet().forEach(p -> query.setParameter(p.getKey(), p.getValue()));
		}
		entityManager.getTransaction().begin();
		query.setFirstResult(first);
		query.setMaxResults(max);
		query.setHint("javax.persistence.cache.storeMode", "REFRESH");
		result = query.getResultList();
		entityManager.flush();
		entityManager.clear();
		entityManager.getTransaction().commit();
		return result;
	}

	@SuppressWarnings("unchecked")
	public E selectSingle(String queryName, Map<String, Object> params) throws Exception {
		E e = null;
		Query query = entityManager.createNamedQuery(queryName);
		if (params != null) {
			for (Entry<String, Object> en : params.entrySet()) {
				query.setParameter(en.getKey(), en.getValue());
			}
		}
		try {
			entityManager.getTransaction().begin();
			query.setHint("javax.persistence.cache.storeMode", "REFRESH");
			e = (E) query.getSingleResult();
			entityManager.getTransaction().commit();
		} catch (NoResultException ex) {
			e = null;
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return e;
	}

	public Float selectCalculat(String queryName, Map<String, Object> params) throws Exception {
		Float e = null;
		Query query = entityManager.createNamedQuery(queryName);
		if (params != null) {
			for (Entry<String, Object> en : params.entrySet()) {
				query.setParameter(en.getKey(), en.getValue());
			}
		}
		try {
			entityManager.getTransaction().begin();
			query.setHint("javax.persistence.cache.storeMode", "REFRESH");
			Object o = query.getSingleResult();
			e = Float.valueOf(String.valueOf(o == null ? "0" : o));
			entityManager.getTransaction().commit();
		} catch (NoResultException ex) {
			return null;
		} finally {
			entityManager.flush();
			entityManager.clear();
		}
		return e;
	}

	@SuppressWarnings("unchecked")
	public List<E> executeNamedQuery(String queryName, Map<String, Object> params) throws Exception {
		List<E> result = new ArrayList<>();
		Query query = entityManager.createNamedQuery(queryName);
		if (params != null) {
			for (Entry<String, Object> e : params.entrySet()) {
				query.setParameter(e.getKey(), e.getValue());
			}
		}
		entityManager.getTransaction().begin();
		query.setHint("javax.persistence.cache.storeMode", "REFRESH");
		result = query.getResultList();
		entityManager.flush();
		entityManager.clear();
		entityManager.getTransaction().commit();
		return result;
	}

	@SuppressWarnings("unchecked")
	public List<E> select(Class<E> e) {
		if (e == null) {
			return null;
		}
		entityManager.getTransaction().begin();
		StringBuffer strQuery = new StringBuffer(
				"SELECT distinct e FROM " + e.getSimpleName() + " AS e  ORDER BY e.id");
		Query query = entityManager.createQuery(strQuery.toString());
		query.setHint("javax.persistence.cache.storeMode", "REFRESH");
		List<E> results = query.getResultList();
		entityManager.getTransaction().commit();
		return results;
	}

	@SuppressWarnings("unchecked")
	public List<E> select(Class<E> e, int first, int max) {
		if (e == null) {
			return null;
		}
		entityManager.getTransaction().begin();
		StringBuffer strQuery = new StringBuffer("SELECT distinct e FROM " + e.getSimpleName() + " AS e ORDER BY e.id");
		Query query = entityManager.createQuery(strQuery.toString());
		query.setFirstResult(first);
		query.setMaxResults(max);
		query.setHint("javax.persistence.cache.storeMode", "REFRESH");
		List<E> results = query.getResultList();
		entityManager.getTransaction().commit();
		return results;
	}

	@SuppressWarnings("unchecked")
	public List<E> select(Class<E> e, int first, int max, String order) {
		if (e == null) {
			return null;
		}
		StringBuffer strQuery = new StringBuffer(
				"SELECT distinct e FROM " + e.getSimpleName() + " AS e ORDER BY e." + order);
		entityManager.getTransaction().begin();
		Query query = entityManager.createQuery(strQuery.toString());
		query.setFirstResult(first);
		query.setMaxResults(max);
		query.setHint("javax.persistence.cache.storeMode", "REFRESH");
		List<E> results = query.getResultList();
		entityManager.getTransaction().commit();
		return results;
	}

	@SuppressWarnings("unchecked")
	public List<E> select(Class<E> e, int first, int max, Map<String, Object> filters, String sortField,
			String sortOrder) throws NoSuchFieldException, SecurityException {
		if (e == null) {
			return null;
		}

		StringBuffer strQuery = new StringBuffer("SELECT distinct e FROM " + e.getSimpleName() + " AS e");
		int filtersize = filters.size();
		if (filters != null && filtersize > 0) {
			strQuery.append(" WHERE ");
			int c = 0;
			for (Entry<String, Object> en : filters.entrySet()) {
				c++;
				Map<String, Class<?>> allFields = getAllFiled(e);

				if (allFields.get(en.getKey()).isPrimitive()) {
					strQuery.append("t." + en.getKey() + " = " + String.valueOf(en.getValue()));
				} else {
					strQuery.append("t." + en.getKey() + " like '" + String.valueOf(en.getValue()) + "%'");
				}

				if (c != filtersize) {
					strQuery.append(" and ");
				}
			}
		}

		if (sortField != null && !sortField.equals("")) {
			strQuery.append("  ORDER BY e." + sortField);
			if (sortOrder != null && !sortOrder.equals("")) {
				strQuery.append(" " + sortOrder);
			}

		}
		entityManager.getTransaction().begin();
		Query query = entityManager.createQuery(strQuery.toString());
		query.setFirstResult(first);
		query.setMaxResults(max);
		query.setHint("javax.persistence.cache.storeMode", "REFRESH");
		List<E> results = query.getResultList();
		entityManager.getTransaction().commit();
		return results;
	}

	@SuppressWarnings("unchecked")
	public List<E> select(Class<E> e, String where, int first, int max) {
		if (e == null) {
			return null;
		}
		if ((where == null) || (where.trim().isEmpty())) {
			return select(e, first, max);
		}
		StringBuffer strQuery = new StringBuffer(
				"SELECT distinct e FROM " + e.getSimpleName() + " AS e WHERE " + where);
		entityManager.getTransaction().begin();
		Query query = entityManager.createQuery(strQuery.toString());
		query.setFirstResult(first);
		query.setMaxResults(max);
		query.setHint("javax.persistence.cache.storeMode", "REFRESH");
		List<E> results = query.getResultList();
		entityManager.getTransaction().commit();
		return results;
	}

	@SuppressWarnings("unchecked")
	public List<E> select(Class<E> e, String afterselect) {
		if (e == null) {
			return null;
		}
		if ((afterselect == null) || (afterselect.trim().isEmpty())) {
			return select(e);
		}
		StringBuffer strQuery = new StringBuffer(
				"SELECT distinct e FROM " + e.getSimpleName() + " AS e " + afterselect);
		entityManager.getTransaction().begin();
		Query query = entityManager.createQuery(strQuery.toString());
		query.setHint("javax.persistence.cache.storeMode", "REFRESH");
		List<E> results = query.getResultList();
		entityManager.getTransaction().commit();
		return results;
	}

	@SuppressWarnings("unchecked")
	public E select(String className, long id) throws Exception {
		Class<E> clazz = (Class<E>) Class.forName(className);
		return select(clazz, id);
	}

	public E select(Class<E> e, long id) {
		return entityManager.find(e, id);
	}

	public float average(Class<E> e, String col, Map<String, String> params) {
		StringBuffer strQuery = new StringBuffer("SELECT AVG(e." + col + ") FROM " + e.getSimpleName() + " AS e ");
		if (params != null) {
			params.entrySet().forEach((p) -> strQuery.append(p.getKey() + p.getValue()));
		}
		entityManager.getTransaction().begin();
		Query query = entityManager.createQuery(strQuery.toString());
		query.setHint("javax.persistence.cache.storeMode", "REFRESH");
		int results = ((Long) query.getSingleResult()).intValue();
		entityManager.getTransaction().commit();
		return results;
	}

	public float sum(Class<E> e, String col, Map<String, String> params) {
		StringBuffer strQuery = new StringBuffer("SELECT sum(e." + col + ") FROM " + e.getSimpleName() + " AS e ");
		if (params != null) {
			params.entrySet().forEach((p) -> strQuery.append(p.getKey() + p.getValue()));
		}
		entityManager.getTransaction().begin();
		Query query = entityManager.createQuery(strQuery.toString());
		query.setHint("javax.persistence.cache.storeMode", "REFRESH");
		float results = ((Long) query.getSingleResult()).intValue();
		entityManager.getTransaction().commit();
		return results;
	}

	public float sum(Class<E> e, String col, String where) {
		StringBuffer strQuery = new StringBuffer("SELECT sum(e." + col + ") FROM " + e.getSimpleName() + " AS e ");
		if (where != null && !where.isEmpty()) {
			strQuery.append(" where " + where);
		}
		entityManager.getTransaction().begin();
		Query query = entityManager.createQuery(strQuery.toString());
		query.setHint("javax.persistence.cache.storeMode", "REFRESH");
		Object o = query.getSingleResult();
		entityManager.getTransaction().commit();
		if (o == null) {
			return 0;
		} else {
			return ((Double) o).floatValue();
		}
	}

	public float max(Class<E> e, String col, Map<String, String> params) {
		StringBuffer strQuery = new StringBuffer("SELECT MAX(e." + col + ") FROM " + e.getSimpleName() + " AS e ");
		if (params != null) {
			params.entrySet().forEach((p) -> strQuery.append(p.getKey() + p.getValue()));
		}
		Query query = entityManager.createQuery(strQuery.toString());
		query.setHint("javax.persistence.cache.storeMode", "REFRESH");
		int results = ((Long) query.getSingleResult()).intValue();
		entityManager.getTransaction().commit();
		return results;
	}

	public float min(Class<E> e, String col, Map<String, String> params) {
		StringBuffer strQuery = new StringBuffer("SELECT MIN(e." + col + ") FROM " + e.getSimpleName() + " AS e ");
		if (params != null) {
			params.entrySet().forEach((p) -> strQuery.append(p.getKey() + p.getValue()));
		}
		entityManager.getTransaction().begin();
		Query query = entityManager.createQuery(strQuery.toString());
		query.setHint("javax.persistence.cache.storeMode", "REFRESH");
		int results = ((Long) query.getSingleResult()).intValue();
		entityManager.getTransaction().commit();
		return results;
	}

	public long count(Class<E> e) {
		entityManager.getTransaction().begin();
		Query query = entityManager.createQuery("SELECT COUNT(e) FROM " + e.getSimpleName() + " AS e");
		query.setHint("javax.persistence.cache.storeMode", "REFRESH");
		long results = (long) query.getSingleResult();
		entityManager.getTransaction().commit();
		return results;
	}

	public long count(Class<E> e, String where) {
		if ((where == null) || (where.equals(""))) {
			return count(e);
		}
		entityManager.getTransaction().begin();
		Query query = entityManager.createQuery("SELECT COUNT(e) FROM " + e.getSimpleName() + " AS e WHERE " + where);
		query.setHint("javax.persistence.cache.storeMode", "REFRESH");
		long results = (long) query.getSingleResult();
		entityManager.getTransaction().commit();
		return results;
	}

	@SuppressWarnings("unchecked")
	public List<Object> executeQuery(String strQuery) {
		entityManager.getTransaction().begin();
		Query query = entityManager.createQuery(strQuery);
		List<Object> results = query.getResultList();
		entityManager.getTransaction().commit();
		return results;
	}

	public long delete(Class<E> e) {
		if (e == null) {
			return 0;
		}
		entityManager.getTransaction().begin();
		Query query = entityManager.createQuery("DELETE FROM " + e.getSimpleName());
		long results = (long) query.executeUpdate();
		entityManager.getTransaction().commit();
		return results;
	}

	public long delete(Class<E> e, long id) {
		if (e == null) {
			return 0;
		}
		entityManager.getTransaction().begin();
		Query query = entityManager.createQuery("DELETE FROM " + e.getSimpleName() + " AS e WHERE e.id = " + id);
		long results = (long) query.executeUpdate();
		entityManager.getTransaction().commit();
		return results;
	}

	public long delete(Class<E> e, String where) {
		if (e == null) {
			return 0;
		}
		if ((where == null) || (where.equals(""))) {
			return delete(e);
		}
		entityManager.getTransaction().begin();
		Query query = entityManager.createQuery("DELETE FROM " + e.getSimpleName() + " AS e WHERE " + where);
		long results = (long) query.executeUpdate();
		entityManager.getTransaction().commit();
		return results;
	}

	public Object executeQuerySingelResult(String strQuery) {
		entityManager.getTransaction().begin();
		Query query = entityManager.createQuery(strQuery);
		Object results = query.getSingleResult();
		entityManager.getTransaction().commit();
		return results;
	}

	public Map<String, Class<?>> getAllFiled(Class<E> clazz) {
		Map<String, Class<?>> result = new HashMap<String, Class<?>>();
		Class<?> temp = clazz;
		do {
			Field[] fields = temp.getDeclaredFields();
			for (Field f : fields) {
				result.put(f.getName(), f.getType());
			}
			temp = temp.getSuperclass();
		} while (temp != null);
		return result;
	}
}