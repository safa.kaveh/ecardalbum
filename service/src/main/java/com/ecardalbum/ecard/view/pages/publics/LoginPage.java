package com.ecardalbum.ecard.view.pages.publics;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/LoginPage")
public class LoginPage extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public LoginPage() {
        super();
    }


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		response.getWriter().print(username);
		if(username.endsWith("ali")&&password.equals("110")) {
			gotoPage("/pages/private/welcome.jsp", request, response);
		}else {
			gotoPage("/pages/public/login.jsp", request, response);
		}
	}

	private void gotoPage(String page, HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		request.getRequestDispatcher(page).forward(request, response);
	}
}
