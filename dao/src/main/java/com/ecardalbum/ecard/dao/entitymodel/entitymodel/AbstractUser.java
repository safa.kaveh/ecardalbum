package com.ecardalbum.ecard.dao.entitymodel.entitymodel;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.ecardalbum.ecard.dao.entitymodel.EntityModel;
import com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractuser.Admin;
import com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractuser.Designer;
import com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractuser.Printary;
import com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractuser.User;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

@Entity
@Table(name = "ABSTRACT_USERS")
@JsonTypeInfo(use = Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @JsonSubTypes.Type(value = Admin.class, name = "Admin"),
		@JsonSubTypes.Type(value = Designer.class, name = "Designer"),
		@JsonSubTypes.Type(value = Printary.class, name = "Printary"),
		@JsonSubTypes.Type(value = User.class, name = "User") })
public abstract class AbstractUser extends EntityModel {
	private static final long serialVersionUID = 1L;
	private byte[] image;
	private String qrcode;
	private long birthday;
	private String firstname;
	private String lastname;
	private String address;
	private long locationLat;
	private long locationLan;

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public String getQrcode() {
		return qrcode;
	}

	public void setQrcode(String qrcode) {
		this.qrcode = qrcode;
	}

	public long getBirthday() {
		return birthday;
	}

	public void setBirthday(long birthday) {
		this.birthday = birthday;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public long getLocationLat() {
		return locationLat;
	}

	public void setLocationLat(long locationLat) {
		this.locationLat = locationLat;
	}

	public long getLocationLan() {
		return locationLan;
	}

	public void setLocationLan(long locationLan) {
		this.locationLan = locationLan;
	}

}
