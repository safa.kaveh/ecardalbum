package com.ecardalbum.ecard.modeldto.dtomodel;

import com.ecardalbum.ecard.modeldto.DtoModel;

public class CardMap extends DtoModel {
	private static final long serialVersionUID = 4392382093400171325L;
	private String description;
	private long locationLat;
	private long locationLan;
	private AbstractCard card;

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public long getLocationLat() {
		return locationLat;
	}

	public void setLocationLat(long locationLat) {
		this.locationLat = locationLat;
	}

	public long getLocationLan() {
		return locationLan;
	}

	public void setLocationLan(long locationLan) {
		this.locationLan = locationLan;
	}

	public AbstractCard getCard() {
		return card;
	}

	public void setCard(AbstractCard card) {
		this.card = card;
	}

}
