package com.ecardalbum.ecard.modeldto.dtomodel;

import com.ecardalbum.ecard.modeldto.DtoModel;
import com.ecardalbum.ecard.modeldto.dtomodel.abstractuser.Admin;
import com.ecardalbum.ecard.modeldto.dtomodel.abstractuser.Designer;
import com.ecardalbum.ecard.modeldto.dtomodel.abstractuser.Printary;
import com.ecardalbum.ecard.modeldto.dtomodel.abstractuser.User;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

@JsonTypeInfo(use = Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @JsonSubTypes.Type(value = Admin.class, name = "Admin"),
		@JsonSubTypes.Type(value = Designer.class, name = "Designer"),
		@JsonSubTypes.Type(value = Printary.class, name = "Printary"),
		@JsonSubTypes.Type(value = User.class, name = "User") })
public abstract class AbstractUser extends DtoModel {
	private static final long serialVersionUID = 1L;
	private byte[] image;
	private String qrcode;
	private long birthday;
	private String firstname;
	private String lastname;
	private String address;
	private long location_lat;
	private long location_lan;

	public byte[] getImage() {
		return image;
	}

	public void setImage(byte[] image) {
		this.image = image;
	}

	public String getQrcode() {
		return qrcode;
	}

	public void setQrcode(String qrcode) {
		this.qrcode = qrcode;
	}

	public long getBirthday() {
		return birthday;
	}

	public void setBirthday(long birthday) {
		this.birthday = birthday;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public long getLocation_lat() {
		return location_lat;
	}

	public void setLocation_lat(long location_lat) {
		this.location_lat = location_lat;
	}

	public long getLocation_lan() {
		return location_lan;
	}

	public void setLocation_lan(long location_lan) {
		this.location_lan = location_lan;
	}

}
