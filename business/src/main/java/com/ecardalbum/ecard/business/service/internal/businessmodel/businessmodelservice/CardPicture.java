package com.ecardalbum.ecard.business.service.internal.businessmodel.businessmodelservice;

import com.ecardalbum.ecard.business.service.internal.businessmodel.BusinessModelService;

public class CardPicture extends
		BusinessModelService<com.ecardalbum.ecard.modeldto.dtomodel.CardPicture, com.ecardalbum.ecard.dao.entitymodel.entitymodel.CardPicture> {

	public CardPicture() {
		super(com.ecardalbum.ecard.modeldto.dtomodel.CardPicture.class,
				com.ecardalbum.ecard.dao.entitymodel.entitymodel.CardPicture.class);

	}

}
