package com.ecardalbum.ecard.business.service.internal.businessmodel;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.ecardalbum.ecard.dao.basic.DaoGeneralEntity;
import com.ecardalbum.ecard.dao.entitymodel.EntityModel;
import com.ecardalbum.ecard.modeldto.DtoModel;

public abstract class BusinessModelService<D extends DtoModel, E extends EntityModel> {
	private static final Logger LOGGER = LogManager.getLogger();

	private Class<D> dtoClass;
	private Class<E> entityClass;

	protected DaoGeneralEntity<E> dao;

	public BusinessModelService(Class<D> dtoClass, Class<E> entityClass) {
		dao = new DaoGeneralEntity<>();
		this.dtoClass = dtoClass;
		this.entityClass = entityClass;
	}

	public D save(D e) throws Exception {
		E entity = dao.save(convertToEntity(e));
		return convertToDto(entity);
	}

	public List<D> save(List<D> es) throws Exception {
		List<D> result = new ArrayList<>();

		es.stream().forEach(e -> {
			try {
				result.add(save(e));
			} catch (Exception e1) {
				LOGGER.error("Business: Can not saved successfully: " + e1.getMessage());
			}
		});
		return result;
	}

	public List<D> saveInOneTransaction(List<D> es) throws Exception {
		List<D> result = new ArrayList<>();

		final List<E> listEntity = new ArrayList<>();
		es.stream().forEach(e -> {
			listEntity.add(convertToEntity(e));
		});
		List<E> el = dao.save(listEntity);

		el.stream().forEach(e -> {
			result.add(convertToDto(e));
		});

		return result;
	}

	public D update(D e) throws Exception {
		E entity = dao.update(convertToEntity(e));
		return convertToDto(entity);
	}

	public List<D> update(List<D> es) throws Exception {
		List<D> result = new ArrayList<>();

		es.stream().forEach(e -> {
			try {
				result.add(update(e));
			} catch (Exception e1) {
				LOGGER.error("Business: Can not update successfully: " + e1.getMessage());
			}
		});
		return result;
	}

	public List<D> updateInOneTransaction(List<D> ts) throws Exception {
		List<D> result = new ArrayList<>();

		final List<E> listEntity = new ArrayList<>();
		ts.stream().forEach(e -> {
			listEntity.add(convertToEntity(e));
		});
		List<E> el = dao.update(listEntity);

		el.stream().forEach(e -> {
			result.add(convertToDto(e));
		});

		return result;
	}

	public D delete(D e) throws Exception {
		E entity = dao.delete(convertToEntity(e));
		return convertToDto(entity);
	}

	public List<D> delete(List<D> ts) throws Exception {
		List<D> result = new ArrayList<>();

		ts.stream().forEach(e -> {
			try {
				result.add(delete(e));
			} catch (Exception e1) {
				LOGGER.error("Business: Can not update successfully: " + e1.getMessage());
			}
		});
		return result;
	}

	public List<D> deleteInOneTransaction(List<D> ts) throws Exception {
		List<D> result = new ArrayList<>();

		final List<E> listEntity = new ArrayList<>();
		ts.stream().forEach(e -> {
			listEntity.add(convertToEntity(e));
		});
		List<E> el = dao.delete(listEntity);

		el.stream().forEach(e -> {
			result.add(convertToDto(e));
		});

		return result;
	}

	public long delete() {
		return dao.delete(entityClass);
	}

	public long delete(long id) {
		return dao.delete(entityClass, id);
	}

	public long delete(String where) {
		return dao.delete(entityClass, where);
	}

	public List<D> select(int first, int max) {
		List<D> result = new ArrayList<>();
		List<E> el = dao.select(entityClass, first, max);

		el.stream().forEach(entity -> result.add(convertToDto(entity)));

		return result;
	}

	public List<D> select(int first, int max, String order) {
		List<D> result = new ArrayList<>();
		List<E> el = dao.select(entityClass, first, max, order);

		el.stream().forEach(entity -> result.add(convertToDto(entity)));

		return result;
	}

	public List<D> select(int first, int max, Map<String, Object> filters, String sortField, String sortOrder)
			throws NoSuchFieldException, SecurityException {
		List<D> result = new ArrayList<>();
		List<E> el = dao.select(entityClass, first, max, filters, sortField, sortOrder);

		el.stream().forEach(entity -> result.add(convertToDto(entity)));

		return result;
	}

	public List<D> select(String where, int first, int max) {
		List<D> result = new ArrayList<>();
		List<E> el = dao.select(entityClass, where, first, max);

		el.stream().forEach(entity -> result.add(convertToDto(entity)));

		return result;
	}

	public List<D> select(String afterselect) {
		List<D> result = new ArrayList<>();
		List<E> el = dao.select(entityClass, afterselect);

		el.stream().forEach(entity -> result.add(convertToDto(entity)));

		return result;
	}

	public D select(String className, long id) throws Exception {
		E e = dao.select(entityClass, id);
		return convertToDto(e);
	}

	public D select(long id) {
		E e = dao.select(entityClass, id);
		return convertToDto(e);
	}

	public float average(String col, Map<String, String> params) {
		return dao.average(entityClass, col, params);
	}

	public float sum(String col, Map<String, String> params) {
		return dao.sum(entityClass, col, params);
	}

	public float sum(String col, String where) {
		return dao.sum(entityClass, col, where);
	}

	public float max(String col, Map<String, String> params) {
		return dao.max(entityClass, col, params);
	}

	public float min(String col, Map<String, String> params) {
		return dao.min(entityClass, col, params);
	}

	public long count() {
		return dao.count(entityClass);
	}

	public long count(String where) {
		return dao.count(entityClass, where);
	}

	public List<Object> executeQuery(String strQuery) {
		return dao.executeQuery(strQuery);
	}

	public Object executeQuerySingelResult(String strQuery) {
		return dao.executeQuerySingelResult(strQuery);
	}

	public E convertToEntity(D dto) {
		return EntityModel.getFromJsonString(dto.toString(), entityClass);
	}

	public D convertToDto(E entity) {
		return DtoModel.getFromJsonString(entity.toString(), dtoClass);
	}

}
