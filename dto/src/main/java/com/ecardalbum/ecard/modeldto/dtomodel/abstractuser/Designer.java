package com.ecardalbum.ecard.modeldto.dtomodel.abstractuser;

import com.ecardalbum.ecard.modeldto.dtomodel.AbstractUser;
import com.fasterxml.jackson.annotation.JsonTypeName;

@JsonTypeName("Designer")
public class Designer extends AbstractUser {
	private static final long serialVersionUID = -2378135597843653577L;

}
