package com.ecardalbum.ecard.dao.entitymodel.entitymodel;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import com.ecardalbum.ecard.dao.entitymodel.EntityModel;
import com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractuser.Designer;
import com.ecardalbum.ecard.dao.entitymodel.entitymodel.abstractuser.Printary;

@Entity
@Table(name = "REL_PRINTARY_DESIGNERS")
public class RelPrintaryDesigner extends EntityModel {
	private static final long serialVersionUID = -8693396765160589803L;

	@OneToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_PRINTARY")
	private Printary printary;

	@OneToOne(cascade = CascadeType.REFRESH, fetch = FetchType.EAGER)
	@JoinColumn(name = "ID_DESIGNER")
	private Designer designer;

	public Printary getPrintary() {
		return printary;
	}

	public void setPrintary(Printary printary) {
		this.printary = printary;
	}

	public Designer getDesigner() {
		return designer;
	}

	public void setDesigner(Designer designer) {
		this.designer = designer;
	}

}
