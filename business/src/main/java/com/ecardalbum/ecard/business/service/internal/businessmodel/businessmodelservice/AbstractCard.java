package com.ecardalbum.ecard.business.service.internal.businessmodel.businessmodelservice;

import com.ecardalbum.ecard.business.service.internal.businessmodel.BusinessModelService;

public abstract class AbstractCard extends
		BusinessModelService<com.ecardalbum.ecard.modeldto.dtomodel.AbstractCard, com.ecardalbum.ecard.dao.entitymodel.entitymodel.AbstractCard> {

	public AbstractCard() {
		super(com.ecardalbum.ecard.modeldto.dtomodel.AbstractCard.class,
				com.ecardalbum.ecard.dao.entitymodel.entitymodel.AbstractCard.class);

	}

}
