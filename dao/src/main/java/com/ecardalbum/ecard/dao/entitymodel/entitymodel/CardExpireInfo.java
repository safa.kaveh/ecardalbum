package com.ecardalbum.ecard.dao.entitymodel.entitymodel;

import javax.persistence.Entity;
import javax.persistence.Table;

import com.ecardalbum.ecard.dao.entitymodel.EntityModel;

@Entity
@Table(name = "CARD_EXPIRE_INFOS")
public class CardExpireInfo extends EntityModel {
	private static final long serialVersionUID = 7607511777994711304L;
	private long expireDate;
	private long warningData;
	private long visitExpireDate;
	private String message;

	public long getExpireDate() {
		return expireDate;
	}

	public void setExpireDate(long expireDate) {
		this.expireDate = expireDate;
	}

	public long getWarningData() {
		return warningData;
	}

	public void setWarningData(long warningData) {
		this.warningData = warningData;
	}

	public long getVisitExpireDate() {
		return visitExpireDate;
	}

	public void setVisitExpireDate(long visitExpireDate) {
		this.visitExpireDate = visitExpireDate;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
